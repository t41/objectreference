<?php

use Maell\View;

use Maell\View\SimpleComponent;
use Maell\Core;
use Maell\Core\Module;
use Maell\ObjectModel\Collection;
use Maell\View\ListComponent;
use Maell\ObjectModel;
use Maell\View\TemplateComponent;

/**
 * IndexController
 *
 * @author
 * @version
 */

require_once 'application/controllers/LoggedController.php';

class Objectreference_IndexController extends LoggedController {
	
	public function preDispatch() {}
	
	
	
	public function indexAction() {
		$this->referenceAction();
	}
	
	public function referenceAction() {
		$this->_objectReference();
	}
	
	protected function _objectReference()
	{
	
		View::setTemplate('objectReference.template.html'); // !!!!!!!!!!!!!!!!
		$list = ObjectModel::getList();
		$menu = '<ol>';
	
		foreach ($list as $k => $v) {
			if (trim($v)=='' || $v=='0') continue;
				
			$objdesc = new objectReference(ObjectModel::getObjectProperties($v), $v);
				
			$rules = ObjectModel::getRules(new $v, TRUE);
			if ($rules!=NULL) $objdesc->setRules($rules);
	
			$tpl = new TemplateComponent();
			$tpl->load('objectReference.dump.template.html'); // !!!!!!!!!!!!!!!!
			$tpl->addVariable('dumpcontent', $objdesc->display());
			$tpl->register();
	
			$link = str_replace('\\', '_', $v);
			$menu .= '<li><a class="object" href="#'.$link.'" title="'.$v.'">'.$v.'</a></li>';
	
		}
	
		$menu .= '</ol>';
		$tpl = new TemplateComponent();
		$tpl->load('objectReference.template.html');
		$tpl->addVariable('dumpcontent', $menu);
		$tpl->register('left');
	
	}
}