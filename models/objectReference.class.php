<?php
class objectReference {

	private $_data = Array();
	private $_label;
	private $_rules = FALSE;
	const BODY_START = '<table class="table table-condensed table-hover">';
	const BODY_START_ALT = '<table class="table table-condensed table-hover alt">';
	const BODY_STOP = '</table>';
	const ROW_START = '<tr>';
	const ROW_STOP = '</tr>';
	const CELL_START = '<td>';
	const CELL_STOP = '</td>';
	const HEAD_START = '<th>';
	const HEAD_STOP = '</th>';
	const TITLE_START = '<caption>';
	const TITLE_STOP = '</caption>';

	function __construct($data, $label) {
		$this->_data = $data;
		$this->_label = $label;
		
	}

	function display() {
		$anchor = str_replace('\\', '_', $this->_label);
		$html = self::BODY_START;
		$html .= self::TITLE_START.'<a class="object" href="#'.$anchor.'" id="'.$anchor.'">'.$this->_label.self::TITLE_STOP;
		$html .= $this->getHeader();

		foreach($this->_data as $k => $data) {
			$html .= self::ROW_START;
			$html .= $this->formatRow($data, $k);
			$html .= self::ROW_STOP;
		}

		$html .= self::BODY_STOP;
		
		if ($this->_rules!=FALSE && is_array($this->_rules)) {
			$html .= $this->formatRules();
		}

		return $html;
	}
	
	function getHeader() {
		$html = self::ROW_START . self::HEAD_START
				. 'Prop. Name / Display Label' 
				. self::HEAD_STOP . self::HEAD_START
				. 'Type'
				. self::HEAD_STOP . self::HEAD_START
				. 'Constraints'
				. self::HEAD_STOP . self::HEAD_START
				. 'Display prop.'
				. self::HEAD_STOP . self::ROW_STOP;

		//$html = '<tr><th>Prop. Name / Display Label</th><th>Type</th><th>Constraints</th><th>Display prop.</th></tr>';
		return $html;
	}

	function formatRow($data, $key) {
		
		$html = self::CELL_START . $this->formatValue($key, 'property');
		if (@is_array($data['label'])) {

			foreach ($data['label'] as $k => $v) {

				$html .= $this->formatValue(' ['.$k . ': ' . $v.']', 'label');
			}
		} else {
			$html .= $this->formatValue(@$data['label'], 'label');
		}


		$html .= self::CELL_STOP . self::CELL_START;

		
		$html .= $this->formatValue(@$data['type'], 'type');
		if (isset($data['instanceof'])) {
			$html .= ' ' . $this->formatValue($data['instanceof'], 'object');

			if (isset($data['keyprop']) && trim($data['keyprop'])!='') {
				$html .= $this->formatValue(array($data['keyprop'], $data['instanceof']), 'member');

			} else if (isset($data['morekeyprop'])) {
				$html .= ' <br /><code>keys: [';
				$i=0;

				foreach ($data['morekeyprop'] as $kp) {
					
					if ($i==0) {
						$html .= $i . ':' . $kp;
					} else {
						$html .= '; ' . $i . ':' .$kp;
					}
					$i++;
				}
				$html .= ']</code> ';
			}
		}


		$html .= self::CELL_STOP . self::CELL_START;


		if (is_array(@$data['constraints'])) {
			foreach ($data['constraints'] as $k => $c) {
				$shtml = $k;
				if (trim($c)!='') $shtml .= ': '. $c;
				$html .= $this->formatValue($shtml, 'constraint');				
			}
		}
		

		$html .= self::CELL_STOP . self::CELL_START;
		

		if (isset($data['display'])) {
			$html .= $this->formatValue(array($data['display'], $data['instanceof']), 'displayproperty');
		}
		
		if (isset($data['values']) && is_array($data['values'])) {
			$html .= '<dl class="dl-horizontal">';
			foreach ($data['values'] as $k => $v) {
				$html .= $this->formatValue(array($k, $v['label']), 'enum');
			}
			$html .= '</dl>';
		}
		$html .= self::CELL_STOP;
		
		return $html;
	}

	function formatValue($value, $type) {
		// return a formatted single cell value
		
		switch ($type) {
			case 'property':
				$link = str_replace('\\', '_', $this->_label) . '-' . $value;
				$value = '<a class="property" href="#'.$link.'" id="'.$link.'"><code>'.$value.'</code></a>';
			break;
				
			case 'displayproperty':
				$value[0] = str_replace('%', '&#37;', $value[0]);
				$link = str_replace('\\', '_', $value[1]);// . '-' . $value;
				$value = '<a class="property" href="#'.$link.'"><code>'.$value[0].'</code></a>';
			break;
				
			case 'label':
				$value = '<span class="objectlabel">'.$value.'</span>';
			break;

			case 'type':
				$value = '<kbd>'.$value.'</kbd>';
			break;
				
			case 'object':
				$link = str_replace('\\', '_', $value);
				$value = '<a class="object" href="#'.$link.'" title="'.$value.'">'.$value.'</a>';
			break;
				
			case 'member':
				$link = str_replace('\\', '_', $value[1]) . '-' . $value[0];
				$value = '<a class="object" href="#' . $link . '" title="' . $value[1] . '->' . $value[0] . '">->' . $value[0] . '</a>';
			break;
				
			case 'constraint':
				$css = 'label ';
				switch ($value) {
					case 'mandatory': $css .= 'label-danger '; break;
					case 'protected': $css .= 'label-primary '; break;
					default: $css .= 'label-default'; break;
				}
				$value = '<span class="'.$css.'">'.$value.'</span>';
			break;
				
			case 'enum':
				$v = '<dt>'.$value[0].'</dt>';
				$v .= '<dd>'.$value[1].'</dd>';
				$value = $v;
			break;
		}

		return $value;
	}
	
	function formatRules() {
		$html = self::BODY_START_ALT;
		$html .= '<caption>'.$this->formatValue($this->_label, 'object').' Rules</caption>';
		
		$html .= self::ROW_START.self::HEAD_START 
				. 'Rule Name (type)'
				. self::HEAD_STOP . self::HEAD_START 
				. 'Source'
				. self::HEAD_STOP . self::HEAD_START
				. 'Trigger' 
				. self::HEAD_STOP . self::ROW_STOP;
		//$html .= '<tr><th>Rule Name (type)</th><th>Source</th><th>Trigger</th></tr>';
		
		foreach ($this->_rules as $k => $v) {
			$html .= self::ROW_START . self::CELL_START;
			
			$html .= '<a class="property nolink"><code>'.$k . '</code></a> <span class="objectlabel">(' . $v['type'] . ')</span>';
			
			$html .= self::CELL_STOP . self::CELL_START;
			
			foreach ($v['source'] as $sk => $sv) {
				$html .= '<span class="objectlabel">' . $sk . ': ' . $sv . '</span>';
			}
			
			$html .= self::CELL_STOP . self::CELL_START;
			
			$html .= '<a class="property nolink"><code>' . $v['trigger']['when'] .' / '. $v['trigger']['event'] .' /</code></a>'. $this->formatValue($v['trigger']['property'], 'property');
			
			
			
			$html .= self::CELL_STOP . self::ROW_STOP;
		}
				
		$html .= self::BODY_STOP;
		return $html;
	}
	
	function setRules($rules) {
		$this->_rules = $rules;
	}

}